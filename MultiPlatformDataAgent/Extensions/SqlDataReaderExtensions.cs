﻿using System;

namespace System.Data.SqlClient
{
    internal static class SqlDataReaderExtensions
    {
        public static bool? GetNullBool(this SqlDataReader reader, int column)
        {
            bool? val = new bool?();

            if (reader.IsDBNull(column))
                return val;
            val = reader.GetBoolean(column);

            return val;
        }

        public static int? GetNullInt32(this SqlDataReader reader, int column)
        {
            int? val = new int?();

            if (reader.IsDBNull(column))
                return val;
            val = reader.GetInt32(column);

            return val;
        }

        public static long? GetNullInt64(this SqlDataReader reader, int column)
        {
            long? val = new long?();

            if (reader.IsDBNull(column))
                return val;
            val = reader.GetInt64(column);

            return val;
        }

        public static double? GetNullDouble(this SqlDataReader reader, int column)
        {
            double? val = new double?();

            if (reader.IsDBNull(column))
                return val;
            val = reader.GetDouble(column);

            return val;
        }

        public static DateTime? GetNullDateTime(this SqlDataReader reader, int column)
        {
            DateTime? val = new DateTime?();

            if (reader.IsDBNull(column))
                return val;
            val = reader.GetDateTime(column);

            return val;
        }

        public static string GetNullString(this SqlDataReader reader, int column)
        {
            string val = null;

            if (reader.IsDBNull(column))
                return val;
            val = reader.GetString(column);

            return val;
        }
    }
}
