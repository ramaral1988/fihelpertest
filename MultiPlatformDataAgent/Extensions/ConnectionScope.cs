﻿using System;

namespace System.Data.SqlClient
{
    internal class ConnectionScope : IDisposable
    {
        private bool isDisposed = false;
        private SqlConnection connection = null;
        private bool keepOpen = false;

        public ConnectionScope(SqlConnection connection)
        {
            this.connection = connection;

            // If the connection is open on entry, do not close it on exit
            keepOpen = connection.State == System.Data.ConnectionState.Open;

            if (connection.State != System.Data.ConnectionState.Open)
                connection.Open();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (isDisposed)
                return;

            if (isDisposing)
            {
                if (connection.State != System.Data.ConnectionState.Closed && !keepOpen)
                    connection.Close();
            }

            isDisposed = true;
        }
    }
}
