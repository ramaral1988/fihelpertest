﻿using MultiPlatformDataAgent.Helpers;
using MultiPlatformDataAgent.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiPlatformDataAgent
{
    public static class MultiPlatformService
    {
        public static StorageRow[] GetStorage(RequestType requestType, int[] ids, DateTime from, DateTime to)
        {
            List<StorageRow> rows = new List<StorageRow>();
            DateTimeRange[] ranges = DateTimeHelper.SplitByDays(from, to, 38);

           foreach(DateTimeRange item in ranges)
            {
                if (item.UseStorage)
                {
                    rows.AddRange(StorageHelper.GetRows(requestType, ids, from, to));
                }
                else
                {
                    rows.AddRange(DatabaseHelper.GetRows(requestType, ids, from, to));
                }
            };

            return rows.ToArray();
        }

        public static StorageRow[] GetParallelStorage(RequestType requestType, int[] ids, DateTime from, DateTime to)
        {
            ConcurrentQueue<StorageRow[]> rows = new ConcurrentQueue<StorageRow[]>();
            DateTimeRange[] ranges = DateTimeHelper.SplitByDays(from, to, 38);

            Parallel.ForEach(ranges, item =>
             {
                 if (item.UseStorage)
                 {
                     rows.Enqueue(StorageHelper.GetRows(requestType, ids, from, to));
                 }
                 else
                 {
                     rows.Enqueue(DatabaseHelper.GetRows(requestType, ids, from, to));
                 }
             });

            return rows.SelectMany(n => n).ToArray();
        }

        public static StorageRowByDistrict[] GetStorageByDistrict(RequestType requestType, int[] ids, DateTime from, DateTime to)
        {
            List<StorageRowByDistrict> rows = new List<StorageRowByDistrict>();
            DateTimeRange[] ranges = DateTimeHelper.SplitByDays(from, to, 38);

            foreach (DateTimeRange item in ranges)
            {
                if (item.UseStorage)
                {
                    rows.AddRange(StorageHelper.GetRowsByDistrict(requestType, ids, from, to));
                }
                else
                {
                    rows.AddRange(DatabaseHelper.GetRowsByDistrict(requestType, ids, from, to));
                }

            }

            return rows.ToArray();
        }

        public static StorageRowByDistrict[] GetParallelStorageByDistrict(RequestType requestType, int[] ids, DateTime from, DateTime to)
        {
            ConcurrentQueue<StorageRowByDistrict[]> rows = new ConcurrentQueue<StorageRowByDistrict[]>();
            DateTimeRange[] ranges = DateTimeHelper.SplitByDays(from, to, 38);

            Parallel.ForEach(ranges, item =>
           {
               if (item.UseStorage)
               {
                   rows.Enqueue(StorageHelper.GetRowsByDistrict(requestType, ids, from, to));
               }
               else
               {
                   rows.Enqueue(DatabaseHelper.GetRowsByDistrict(requestType, ids, from, to));
               }

           });

            return rows.SelectMany(n=> n).ToArray();
        }
    }
}
