﻿namespace MultiPlatformDataAgent.Models
{
    public enum RequestType
    {
        Vehicles = 1,
        Drivers,
        CoDrivers
    }
}
