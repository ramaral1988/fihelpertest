﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiPlatformDataAgent.Models
{
    public interface IStorageRowByDistrict : IStorageRow
    {
        string Country { get; set; }
        string District { get; set; }
    }
}
