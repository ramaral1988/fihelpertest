﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiPlatformDataAgent.Models
{
    public class StorageRow : IStorageRow
    {
        public StorageRow()
        {

        }

        public StorageRow(DateTime rowTimestamp, int companyId, int vehicleId, int driverId, int coDriverId)
        {
            RowTimeStamp = new DateTime(rowTimestamp.Year, rowTimestamp.Month, rowTimestamp.Day, rowTimestamp.Hour, 0, 0);
            CompanyId = companyId;
            VehicleId = vehicleId;
            DriverId = driverId;
            CoDriverId = coDriverId;
        }

        public string RowKey
        {
            get
            {
                return GetRowKey(RowTimeStamp, CompanyId, VehicleId, DriverId, CoDriverId);
            }
            set
            {
            }
        }
        public DateTime RowTimeStamp { get; set; } //KEY
        public int CompanyId { get; set; } //KEY
        public int VehicleId { get; set; } //KEY
        public int DriverId { get; set; } //KEY
        public int CoDriverId { get; set; } //KEY
        public double? CommonMileage { get; set; }
        public double? CommonDrivingTime { get; set; }
        public double? CommonIdleTime { get; set; }
        public double? CommonIgnitionTime { get; set; }
        public int? CommonTrips { get; set; }
        public bool? CommonTripOverlap { get; set; }
        public double? CommonFuelTotal { get; set; }
        public double? CommonSpeedSum { get; set; }
        public int? CommonSpeedMax { get; set; }
        public int? CommonSamples { get; set; }
        public double? CanbusMileage { get; set; }
        public double? CanbusEngineSpeedMax { get; set; }
        public double? CanbusEngineTempMax { get; set; }
        public int? CanbusInfractions { get; set; }
        public int? CanbusHiTorqueTime { get; set; }
        public int? CanbusHiSpeedTime { get; set; }
        public int? CanbusHiRPMTime { get; set; }
        public int? CanbusHiAccelTime { get; set; }
        public int? CanbusWorkStates { get; set; }
        public bool? CanbusWorkStateOverlap { get; set; }
        public double? CanbusFuelTotal { get; set; }
        public double? CanbusFuelInIdle { get; set; }
        public double? CanbusTachoDrivingTime { get; set; }
        public double? CanbusTachoWorkTime { get; set; }
        public double? CanbusTachoAvailableTime { get; set; }
        public double? CanbusTachoRestTime { get; set; }
        public double? EcoIdleTime { get; set; }
        public double? EcoSpeedingTime { get; set; }
        public double? EcoNoCruiseTime { get; set; }
        public double? EcoHighRPMTime { get; set; }
        public double? EcoIdleTimeMax { get; set; }
        public double? EcoSpeedingTimeMax { get; set; }
        public double? EcoNoCruiseTimeMax { get; set; }
        public double? EcoHighRPMTimeMax { get; set; }
        public int? EcoHiSpeedAccel { get; set; }
        public int? EcoLoSpeedAccel { get; set; }
        public int? EcoHiSpeedBrake { get; set; }
        public int? EcoLoSpeedBrake { get; set; }
        public double? EcoHiSpeedAccelMax { get; set; }
        public double? EcoLoSpeedAccelMax { get; set; }
        public double? EcoHiSpeedBrakeMax { get; set; }
        public double? EcoLoSpeedBrakeMax { get; set; }
        public double? EcoHighRPMTimeOverlap { get; set; }
        public double? EcoSpeedingTimeOverlap { get; set; }
        public double? EcoIdleTimeOverlap { get; set; }
        public double? EcoNoCruiseTimeOverlap { get; set; }

        public static string GetRowKey(DateTime rowTimestamp, int companyId, int vehicleId, int driverId, int coDriverId)
        {
            return $"T{rowTimestamp.ToString("yyyyMMddHH")}_A{companyId}_V{vehicleId}_D{driverId}_CD{coDriverId}";
        }
    }
}

