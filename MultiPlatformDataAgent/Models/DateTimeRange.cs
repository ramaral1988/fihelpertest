﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiPlatformDataAgent.Models
{
    internal class DateTimeRange
    {
        public DateTimeRange()
        {
        }

        public DateTimeRange(DateTime start, DateTime end, bool useStorage = false)
        {
            Start = start;
            End = end;
            UseStorage = useStorage;
        }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public bool UseStorage { get; set; }

        public bool Contains(DateTime dt)
        {
            return Start <= dt && dt <= End;
        }
    }
}
