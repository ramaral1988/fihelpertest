﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiPlatformDataAgent.Models
{
    public interface IStorageRow
    {
        string RowKey { get; set; }
        DateTime RowTimeStamp { get; set; }
        int CompanyId { get; set; }
        int VehicleId { get; set; }
        int DriverId { get; set; }
        int CoDriverId { get; set; }
        double? CommonMileage { get; set; }
        double? CommonDrivingTime { get; set; }
        double? CommonIdleTime { get; set; }
        double? CommonIgnitionTime { get; set; }
        int? CommonTrips { get; set; }
        bool? CommonTripOverlap { get; set; }
        double? CommonFuelTotal { get; set; }
        double? CommonSpeedSum { get; set; }
        int? CommonSpeedMax { get; set; }
        int? CommonSamples { get; set; }
        double? CanbusMileage { get; set; }
        double? CanbusEngineSpeedMax { get; set; }
        double? CanbusEngineTempMax { get; set; }
        int? CanbusInfractions { get; set; }
        int? CanbusHiTorqueTime { get; set; }
        int? CanbusHiSpeedTime { get; set; }
        int? CanbusHiRPMTime { get; set; }
        int? CanbusHiAccelTime { get; set; }
        int? CanbusWorkStates { get; set; }
        bool? CanbusWorkStateOverlap { get; set; }
        double? CanbusFuelTotal { get; set; }
        double? CanbusFuelInIdle { get; set; }
        double? CanbusTachoDrivingTime { get; set; }
        double? CanbusTachoWorkTime { get; set; }
        double? CanbusTachoAvailableTime { get; set; }
        double? CanbusTachoRestTime { get; set; }
        double? EcoIdleTime { get; set; }
        double? EcoSpeedingTime { get; set; }
        double? EcoNoCruiseTime { get; set; }
        double? EcoHighRPMTime { get; set; }
        double? EcoIdleTimeMax { get; set; }
        double? EcoSpeedingTimeMax { get; set; }
        double? EcoNoCruiseTimeMax { get; set; }
        double? EcoHighRPMTimeMax { get; set; }
        int? EcoHiSpeedAccel { get; set; }
        int? EcoLoSpeedAccel { get; set; }
        int? EcoHiSpeedBrake { get; set; }
        int? EcoLoSpeedBrake { get; set; }
        double? EcoHiSpeedAccelMax { get; set; }
        double? EcoLoSpeedAccelMax { get; set; }
        double? EcoHiSpeedBrakeMax { get; set; }
        double? EcoLoSpeedBrakeMax { get; set; }
        double? EcoHighRPMTimeOverlap { get; set; }
        double? EcoSpeedingTimeOverlap { get; set; }
        double? EcoIdleTimeOverlap { get; set; }
        double? EcoNoCruiseTimeOverlap { get; set; }
    }
}
