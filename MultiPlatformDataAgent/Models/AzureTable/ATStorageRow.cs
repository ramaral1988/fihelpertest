﻿using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace MultiPlatformDataAgent.Models.AzureTable
{
    internal class ATStorageRow : TableEntity, IStorageRow
    {
        public ATStorageRow()
        {

        }

        public DateTime RowTimeStamp { get; set; }
        public int CompanyId { get; set; }
        public int VehicleId { get; set; }
        public int DriverId { get; set; }
        public int CoDriverId { get; set; }
        public double? CommonMileage { get; set; }
        public double? CommonDrivingTime { get; set; }
        public double? CommonIdleTime { get; set; }
        public double? CommonIgnitionTime { get; set; }
        public int? CommonTrips { get; set; }
        public bool? CommonTripOverlap { get; set; }
        public double? CommonFuelTotal { get; set; }
        public double? CommonSpeedSum { get; set; }
        public int? CommonSpeedMax { get; set; }
        public int? CommonSamples { get; set; }
        public double? CanbusMileage { get; set; }
        public double? CanbusEngineSpeedMax { get; set; }
        public double? CanbusEngineTempMax { get; set; }
        public int? CanbusInfractions { get; set; }
        public int? CanbusHiTorqueTime { get; set; }
        public int? CanbusHiSpeedTime { get; set; }
        public int? CanbusHiRPMTime { get; set; }
        public int? CanbusHiAccelTime { get; set; }
        public int? CanbusWorkStates { get; set; }
        public bool? CanbusWorkStateOverlap { get; set; }
        public double? CanbusFuelTotal { get; set; }
        public double? CanbusFuelInIdle { get; set; }
        public double? CanbusTachoDrivingTime { get; set; }
        public double? CanbusTachoWorkTime { get; set; }
        public double? CanbusTachoAvailableTime { get; set; }
        public double? CanbusTachoRestTime { get; set; }
        public double? EcoIdleTime { get; set; }
        public double? EcoSpeedingTime { get; set; }
        public double? EcoNoCruiseTime { get; set; }
        public double? EcoHighRPMTime { get; set; }
        public double? EcoIdleTimeMax { get; set; }
        public double? EcoSpeedingTimeMax { get; set; }
        public double? EcoNoCruiseTimeMax { get; set; }
        public double? EcoHighRPMTimeMax { get; set; }
        public int? EcoHiSpeedAccel { get; set; }
        public int? EcoLoSpeedAccel { get; set; }
        public int? EcoHiSpeedBrake { get; set; }
        public int? EcoLoSpeedBrake { get; set; }
        public double? EcoHiSpeedAccelMax { get; set; }
        public double? EcoLoSpeedAccelMax { get; set; }
        public double? EcoHiSpeedBrakeMax { get; set; }
        public double? EcoLoSpeedBrakeMax { get; set; }
        public double? EcoHighRPMTimeOverlap { get; set; }
        public double? EcoSpeedingTimeOverlap { get; set; }
        public double? EcoIdleTimeOverlap { get; set; }
        public double? EcoNoCruiseTimeOverlap { get; set; }

        public StorageRow GetStorageRow()
        {
            return new StorageRow()
            {
                RowTimeStamp = this.RowTimeStamp,
                CompanyId = this.CompanyId,
                VehicleId = this.VehicleId,
                DriverId = this.DriverId,
                CoDriverId = this.CoDriverId,
                CommonMileage = this.CommonMileage,
                CommonDrivingTime = this.CommonDrivingTime,
                CommonIdleTime = this.CommonIdleTime,
                CommonIgnitionTime = this.CommonIgnitionTime,
                CommonTrips = this.CommonTrips,
                CommonTripOverlap = this.CommonTripOverlap,
                CommonFuelTotal = this.CommonFuelTotal,
                CommonSpeedSum = this.CommonSpeedSum,
                CommonSpeedMax = this.CommonSpeedMax,
                CommonSamples = this.CommonSamples,
                CanbusMileage = this.CanbusMileage,
                CanbusEngineSpeedMax = this.CanbusEngineSpeedMax,
                CanbusEngineTempMax = this.CanbusEngineTempMax,
                CanbusInfractions = this.CanbusInfractions,
                CanbusHiTorqueTime = this.CanbusHiTorqueTime,
                CanbusHiSpeedTime = this.CanbusHiSpeedTime,
                CanbusHiRPMTime = this.CanbusHiRPMTime,
                CanbusHiAccelTime = this.CanbusHiAccelTime,
                CanbusWorkStates = this.CanbusWorkStates,
                CanbusWorkStateOverlap = this.CanbusWorkStateOverlap,
                CanbusFuelTotal = this.CanbusFuelTotal,
                CanbusFuelInIdle = this.CanbusFuelInIdle,
                CanbusTachoDrivingTime = this.CanbusTachoDrivingTime,
                CanbusTachoWorkTime = this.CanbusTachoWorkTime,
                CanbusTachoAvailableTime = this.CanbusTachoAvailableTime,
                CanbusTachoRestTime = this.CanbusTachoRestTime,
                EcoIdleTime = this.EcoIdleTime,
                EcoSpeedingTime = this.EcoSpeedingTime,
                EcoNoCruiseTime = this.EcoNoCruiseTime,
                EcoHighRPMTime = this.EcoHighRPMTime,
                EcoIdleTimeMax = this.EcoIdleTimeMax,
                EcoSpeedingTimeMax = this.EcoSpeedingTimeMax,
                EcoNoCruiseTimeMax = this.EcoNoCruiseTimeMax,
                EcoHighRPMTimeMax = this.EcoHighRPMTimeMax,
                EcoHiSpeedAccel = this.EcoHiSpeedAccel,
                EcoLoSpeedAccel = this.EcoLoSpeedAccel,
                EcoHiSpeedBrake = this.EcoHiSpeedBrake,
                EcoLoSpeedBrake = this.EcoLoSpeedBrake,
                EcoHiSpeedAccelMax = this.EcoHiSpeedAccelMax,
                EcoLoSpeedAccelMax = this.EcoLoSpeedAccelMax,
                EcoHiSpeedBrakeMax = this.EcoHiSpeedBrakeMax,
                EcoLoSpeedBrakeMax = this.EcoLoSpeedBrakeMax,
                EcoHighRPMTimeOverlap = this.EcoHighRPMTimeOverlap,
                EcoSpeedingTimeOverlap = this.EcoSpeedingTimeOverlap,
                EcoIdleTimeOverlap = this.EcoIdleTimeOverlap,
                EcoNoCruiseTimeOverlap = this.EcoNoCruiseTimeOverlap
            };
        }
    }
}