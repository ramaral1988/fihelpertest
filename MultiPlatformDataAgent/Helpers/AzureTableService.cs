﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using MultiPlatformDataAgent.Models.AzureTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiPlatformDataAgent.Helpers
{
    internal class AzureTableService
    {
        private CloudStorageAccount storageAccount = null;
        private CloudTableClient tableClient = null;
        private CloudTable table = null;

        public string StorageConnectionString { get; set; }

        private AzureTableService() { }

        public AzureTableService(string connectionString, string tableName)
        {
            StorageConnectionString = connectionString;
            CreateTable(tableName);
        }

        public void CreateTable(string tableName)
        {
            table = GetTable(tableName);
        }

        public CloudTable GetTable(string tableName)
        {
            CloudTable azrTable = null;

            if (storageAccount == null)
            {
                storageAccount = CloudStorageAccount.Parse(StorageConnectionString);
            }

            if (tableClient == null)
            {
                tableClient = storageAccount.CreateCloudTableClient();
            }

            if (azrTable == null)
            {
                azrTable = tableClient.GetTableReference(tableName);
            }

            return azrTable;
        }

        public ATStorageRow[] GetStorageRow(TableQuery<ATStorageRow> query)
        {
            return table.ExecuteQuery(query).ToArray();
        }

        public ATStorageRowByDistrict[] GetStorageRowByDistrict(TableQuery<ATStorageRowByDistrict> query)
        {
            return table.ExecuteQuery(query).ToArray();
        }
    }
}
