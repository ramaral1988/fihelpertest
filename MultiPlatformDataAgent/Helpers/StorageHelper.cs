﻿using Microsoft.WindowsAzure.Storage.Table;
using MultiPlatformDataAgent.Models;
using MultiPlatformDataAgent.Models.AzureTable;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiPlatformDataAgent.Helpers
{
    internal class StorageHelper
    {
        internal const string ConnectionString = "DefaultEndpointsProtocol=https;AccountName=prdfistg08;AccountKey=EbIMheNyZQPDeQgJ2BNIh8jSkrbqBo8Dvo5ZzulvTpa8Nj52YPHvxSz2r/2Kzys/atnmA8Noiu8QN5rzdthoVQ==;EndpointSuffix=core.windows.net";

        internal const string TableName = "StorageData";

        internal const string TableNameByDistrict = "StorageDataByDistrict";

        internal static StorageRow[] GetRows(RequestType requestType, int[] ids, DateTime from, DateTime to)
        {
            AzureTableService storage = new AzureTableService(ConnectionString, TableName);
            ConcurrentDictionary<int, ATStorageRow[]> results = new ConcurrentDictionary<int, ATStorageRow[]>();

            try
            {
                Parallel.ForEach(ids, id =>
                {
                    string partitionKey = $"V{id}";

                    switch (requestType)
                    {
                        case RequestType.Vehicles:
                            partitionKey = $"V{id}";
                            break;
                        case RequestType.Drivers:
                            partitionKey = $"D{id}";
                            break;
                        case RequestType.CoDrivers:
                            partitionKey = $"C{id}";
                            break;
                    }

                    string query = $"PartitionKey eq '{partitionKey}' and RowKey ge '{from.ToString("yyyyMMddHH")}' and RowKey le '{to.AddHours(1).ToString("yyyyMMddHH")}'";
                    TableQuery<ATStorageRow> tableQuery = new TableQuery<ATStorageRow>().Where(query);
                    ATStorageRow[] rows = storage.GetStorageRow(tableQuery);
                    results.AddOrUpdate(id, rows, (key, val) => rows);
                });

                return results.SelectMany(n => n.Value).Select(n => n.GetStorageRow()).ToArray();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        internal static StorageRowByDistrict[] GetRowsByDistrict(RequestType requestType, int[] ids, DateTime from, DateTime to)
        {
            AzureTableService storage = new AzureTableService(ConnectionString, TableNameByDistrict);
            ConcurrentDictionary<int, ATStorageRowByDistrict[]> results = new ConcurrentDictionary<int, ATStorageRowByDistrict[]>();

            try
            {
                Parallel.ForEach(ids, id =>
                {
                    string partitionKey = $"V{id}";

                    switch (requestType)
                    {
                        case RequestType.Vehicles:
                            partitionKey = $"V{id}";
                            break;
                        case RequestType.Drivers:
                            partitionKey = $"D{id}";
                            break;
                        case RequestType.CoDrivers:
                            partitionKey = $"C{id}";
                            break;
                    }

                    string query = $"PartitionKey eq '{partitionKey}' and RowKey ge '{from.ToString("yyyyMMddHH")}' and RowKey le '{to.AddHours(1).ToString("yyyyMMddHH")}'";
                    TableQuery<ATStorageRowByDistrict> tableQuery = new TableQuery<ATStorageRowByDistrict>().Where(query);
                    ATStorageRowByDistrict[] rows = storage.GetStorageRowByDistrict(tableQuery);
                    results.AddOrUpdate(id, rows, (key, val) => rows);
                });

                return results.SelectMany(n => n.Value).Select(n => n.GetStorageRowByDistrict()).ToArray();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
