﻿using MultiPlatformDataAgent.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiPlatformDataAgent.Helpers
{
    internal class DatabaseHelper
    {
        internal const string ConnectionString = "Server=tcp:prdfisql06.database.windows.net,1433;Initial Catalog=StorageHotData;Persist Security Info=False;User ID=FrotcomAdmin;Password=Fr0tc0m4dm1n;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        internal static StorageRow[] GetRows(RequestType requestType, int[] ids, DateTime from, DateTime to)
        {
            List<StorageRow> list = new List<StorageRow>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {

                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        switch (requestType)
                        {
                            case RequestType.Vehicles:
                                cmd.CommandText = "GetStorageDataByVehicles";
                                cmd.Parameters.Add("@vehicleIds", SqlDbType.NVarChar).Value = string.Join(";", ids);
                                break;
                            case RequestType.Drivers:
                                cmd.CommandText = "GetStorageDataByDrivers";
                                cmd.Parameters.Add("@driversIds", SqlDbType.NVarChar).Value = string.Join(";", ids);
                                break;
                            case RequestType.CoDrivers:
                                cmd.CommandText = "GetStorageDataByCoDrivers";
                                cmd.Parameters.Add("@coDriversIds", SqlDbType.NVarChar).Value = string.Join(";", ids);
                                break;
                        }

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = connection;
                        cmd.Parameters.Add("@from", SqlDbType.DateTime).Value = from;
                        cmd.Parameters.Add("@to", SqlDbType.DateTime).Value = to;

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                StorageRow row = ReadStorageRow(reader);

                                list.Add(row);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    connection.Close();
                }
            }

            return list.ToArray();
        }

        internal static StorageRowByDistrict[] GetRowsByDistrict(RequestType requestType, int[] ids, DateTime from, DateTime to)
        {
            List<StorageRowByDistrict> list = new List<StorageRowByDistrict>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {

                try
                {

                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        switch (requestType)
                        {
                            case RequestType.Vehicles:
                                cmd.CommandText = "GetStorageDataByDistrictByVehicles";
                                cmd.Parameters.Add("@vehicleIds", SqlDbType.NVarChar).Value = string.Join(";", ids);
                                break;
                            case RequestType.Drivers:
                                cmd.CommandText = "GetStorageDataByDistrictByDrivers";
                                cmd.Parameters.Add("@driversIds", SqlDbType.NVarChar).Value = string.Join(";", ids);
                                break;
                            case RequestType.CoDrivers:
                                cmd.CommandText = "GetStorageDataByDistrictByCoDrivers";
                                cmd.Parameters.Add("@coDriversIds", SqlDbType.NVarChar).Value = string.Join(";", ids);
                                break;
                        }

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = connection;
                        cmd.Parameters.Add("@from", SqlDbType.DateTime).Value = from;
                        cmd.Parameters.Add("@to", SqlDbType.DateTime).Value = to;

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                StorageRowByDistrict row = ReadStorageRowByDistrict(reader);

                                list.Add(row);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    connection.Close();
                }
            }
            return list.ToArray();
        }


        private static StorageRow ReadStorageRow(SqlDataReader reader)
        {
            StorageRow row = new StorageRow();
            int c = 0;

            row.RowTimeStamp = reader.GetDateTime(c++);
            row.CompanyId = reader.GetInt32(c++);
            row.VehicleId = reader.GetInt32(c++);
            row.DriverId = reader.GetInt32(c++);
            row.CoDriverId = reader.GetInt32(c++);
            row.RowKey = reader.GetString(c++);
            row.CommonMileage = reader.GetNullDouble(c++);
            row.CommonDrivingTime = reader.GetNullDouble(c++);
            row.CommonIdleTime = reader.GetNullDouble(c++);
            row.CommonIgnitionTime = reader.GetNullDouble(c++);
            row.CommonTrips = reader.GetNullInt32(c++);
            row.CommonTripOverlap = reader.GetNullBool(c++);
            row.CommonFuelTotal = reader.GetNullDouble(c++);
            row.CommonSpeedSum = reader.GetNullDouble(c++);
            row.CommonSpeedMax = reader.GetNullInt32(c++);
            row.CommonSamples = reader.GetNullInt32(c++);
            row.CanbusMileage = reader.GetNullDouble(c++);
            row.CanbusEngineSpeedMax = reader.GetNullDouble(c++);
            row.CanbusEngineTempMax = reader.GetNullDouble(c++);
            row.CanbusInfractions = reader.GetNullInt32(c++);
            row.CanbusHiTorqueTime = reader.GetNullInt32(c++);
            row.CanbusHiSpeedTime = reader.GetNullInt32(c++);
            row.CanbusHiRPMTime = reader.GetNullInt32(c++);
            row.CanbusHiAccelTime = reader.GetNullInt32(c++);
            row.CanbusWorkStates = reader.GetNullInt32(c++);
            row.CanbusWorkStateOverlap = reader.GetNullBool(c++);
            row.CanbusFuelTotal = reader.GetNullDouble(c++);
            row.CanbusFuelInIdle = reader.GetNullDouble(c++);
            row.CanbusTachoDrivingTime = reader.GetNullDouble(c++);
            row.CanbusTachoWorkTime = reader.GetNullDouble(c++);
            row.CanbusTachoAvailableTime = reader.GetNullDouble(c++);
            row.CanbusTachoRestTime = reader.GetNullDouble(c++);
            row.EcoIdleTime = reader.GetNullDouble(c++);
            row.EcoSpeedingTime = reader.GetNullDouble(c++);
            row.EcoNoCruiseTime = reader.GetNullDouble(c++);
            row.EcoHighRPMTime = reader.GetNullDouble(c++);
            row.EcoIdleTimeMax = reader.GetNullDouble(c++);
            row.EcoSpeedingTimeMax = reader.GetNullDouble(c++);
            row.EcoNoCruiseTimeMax = reader.GetNullDouble(c++);
            row.EcoHighRPMTimeMax = reader.GetNullDouble(c++);
            row.EcoHiSpeedAccel = reader.GetNullInt32(c++);
            row.EcoLoSpeedAccel = reader.GetNullInt32(c++);
            row.EcoHiSpeedBrake = reader.GetNullInt32(c++);
            row.EcoLoSpeedBrake = reader.GetNullInt32(c++);
            row.EcoHiSpeedAccelMax = reader.GetNullDouble(c++);
            row.EcoLoSpeedAccelMax = reader.GetNullDouble(c++);
            row.EcoHiSpeedBrakeMax = reader.GetNullDouble(c++);
            row.EcoLoSpeedBrakeMax = reader.GetNullDouble(c++);
            row.EcoHighRPMTimeOverlap = reader.GetNullDouble(c++);
            row.EcoSpeedingTimeOverlap = reader.GetNullDouble(c++);
            row.EcoIdleTimeOverlap = reader.GetNullDouble(c++);
            row.EcoNoCruiseTimeOverlap = reader.GetNullDouble(c++);

            return row;
        }

        private static StorageRowByDistrict ReadStorageRowByDistrict(SqlDataReader reader)
        {
            StorageRowByDistrict row = new StorageRowByDistrict();
            int c = 0;

            row.RowTimeStamp = reader.GetDateTime(c++);
            row.CompanyId = reader.GetInt32(c++);
            row.VehicleId = reader.GetInt32(c++);
            row.DriverId = reader.GetInt32(c++);
            row.CoDriverId = reader.GetInt32(c++);
            row.Country = reader.GetString(c++);
            row.District = reader.GetString(c++);
            row.RowKey = reader.GetString(c++);
            row.CommonMileage = reader.GetNullDouble(c++);
            row.CommonDrivingTime = reader.GetNullDouble(c++);
            row.CommonIdleTime = reader.GetNullDouble(c++);
            row.CommonIgnitionTime = reader.GetNullDouble(c++);
            row.CommonTrips = reader.GetNullInt32(c++);
            row.CommonTripOverlap = reader.GetNullBool(c++);
            row.CommonFuelTotal = reader.GetNullDouble(c++);
            row.CommonSpeedSum = reader.GetNullDouble(c++);
            row.CommonSpeedMax = reader.GetNullInt32(c++);
            row.CommonSamples = reader.GetNullInt32(c++);
            row.CanbusMileage = reader.GetNullDouble(c++);
            row.CanbusEngineSpeedMax = reader.GetNullDouble(c++);
            row.CanbusEngineTempMax = reader.GetNullDouble(c++);
            row.CanbusInfractions = reader.GetNullInt32(c++);
            row.CanbusHiTorqueTime = reader.GetNullInt32(c++);
            row.CanbusHiSpeedTime = reader.GetNullInt32(c++);
            row.CanbusHiRPMTime = reader.GetNullInt32(c++);
            row.CanbusHiAccelTime = reader.GetNullInt32(c++);
            row.CanbusWorkStates = reader.GetNullInt32(c++);
            row.CanbusWorkStateOverlap = reader.GetNullBool(c++);
            row.CanbusFuelTotal = reader.GetNullDouble(c++);
            row.CanbusFuelInIdle = reader.GetNullDouble(c++);
            row.CanbusTachoDrivingTime = reader.GetNullDouble(c++);
            row.CanbusTachoWorkTime = reader.GetNullDouble(c++);
            row.CanbusTachoAvailableTime = reader.GetNullDouble(c++);
            row.CanbusTachoRestTime = reader.GetNullDouble(c++);
            row.EcoIdleTime = reader.GetNullDouble(c++);
            row.EcoSpeedingTime = reader.GetNullDouble(c++);
            row.EcoNoCruiseTime = reader.GetNullDouble(c++);
            row.EcoHighRPMTime = reader.GetNullDouble(c++);
            row.EcoIdleTimeMax = reader.GetNullDouble(c++);
            row.EcoSpeedingTimeMax = reader.GetNullDouble(c++);
            row.EcoNoCruiseTimeMax = reader.GetNullDouble(c++);
            row.EcoHighRPMTimeMax = reader.GetNullDouble(c++);
            row.EcoHiSpeedAccel = reader.GetNullInt32(c++);
            row.EcoLoSpeedAccel = reader.GetNullInt32(c++);
            row.EcoHiSpeedBrake = reader.GetNullInt32(c++);
            row.EcoLoSpeedBrake = reader.GetNullInt32(c++);
            row.EcoHiSpeedAccelMax = reader.GetNullDouble(c++);
            row.EcoLoSpeedAccelMax = reader.GetNullDouble(c++);
            row.EcoHiSpeedBrakeMax = reader.GetNullDouble(c++);
            row.EcoLoSpeedBrakeMax = reader.GetNullDouble(c++);
            row.EcoHighRPMTimeOverlap = reader.GetNullDouble(c++);
            row.EcoSpeedingTimeOverlap = reader.GetNullDouble(c++);
            row.EcoIdleTimeOverlap = reader.GetNullDouble(c++);
            row.EcoNoCruiseTimeOverlap = reader.GetNullDouble(c++);

            return row;
        }
    }
}
