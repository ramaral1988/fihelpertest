﻿using MultiPlatformDataAgent.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiPlatformDataAgent.Helpers
{
    internal class DateTimeHelper
    {
        internal static DateTimeRange[] SplitByDays(DateTime startDate, DateTime endDate, int numberOfDays = 30)
        {
            DateTime todayMinusDays = DateTime.UtcNow.AddDays(-numberOfDays);
            List<DateTimeRange> result = new List<DateTimeRange>();

            if (startDate > todayMinusDays)
            {
                result.Add(new DateTimeRange(startDate, endDate));
            }
            else if (endDate < todayMinusDays)
            {
                result.Add(new DateTimeRange(startDate, endDate, true));
            }
            else
            {
                result.Add(new DateTimeRange(startDate, todayMinusDays.AddSeconds(-1), true));
                result.Add(new DateTimeRange(todayMinusDays, endDate));
            }

            return result.ToArray();
        }
    }
}
